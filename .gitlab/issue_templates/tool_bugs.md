# Tool Bug Report

This is only to report issues with Tools. For a general steps on what to do before reporting, see [General Troubleshooting Guide](https://docs.google.com/document/d/12aNBxbvNkvyKC8Lj2sJ5RN2qbuuVSNdyyRmQ9cEjgII/edit#heading=h.n9fbpd5ls3su).

Where applicable replace `<XYZ>` with your responses (Including your angled brackets)

ex.   `**Browser Name:** <BROWSER NAME>` --> `**Browser Name:** Chrome`

## Tool: <Name of tool here>

## Expected Behaviour

<WHAT SHOULD HAPPEN>

## Current Behaviour

<WHAT ACTUALLY HAPPENS>

## Steps to Reproduce

_Please provide an ordered set of concrete steps to reproduce the issue._

1.
2.
3.

<ADD STEPS AS NECESSARY>

## Relevant logs and/or screenshots

_Paste any relevant logs - please use code blocks (\`\`\`ERROR MSG\`\`\`) to format console output, logs, and code as it's very hard to read otherwise._
Ex. ```ERROR MSG```

## Steps taken to debug:

_Put an X in [ ] for each step to debug you've attempted. You can add comments as necessary_

- [ ] **Can you replicate it?**
    - [ ] Can you reproduce it in the exact same location?
    - [ ] Can you reproduce it with a different file/form?
    - [ ] Can you reproduce it in a different browser? <BROWSER NAME>
- [ ] **Can someone else replicate it?**
- [ ] **Browser troubleshooting Part 1**
    - [ ] Restart the current  browser and see if that fixes it. If not, do the following
        - [ ] Clear cache and cookies
        - [ ] Restart the browser
        - [ ] Try again
- [ ] **Browser troubleshooting Part 2**
    - [ ] Disable browser extensions
    - [ ] Restart browser
    - [ ] Try again
- [ ] **Machine Troubleshooting**
    - [ ] Restart machine
    - [ ] Try again


## Your Environment

**URL:** <URL>

**Browser Name:** <NAME>

**Browser Version:** <BROWSER VERSION>

**Operating System:** <OS>

**Operation System Version:** <OS VERSION>

**Time/Date it happened (be as precise as possible)**: <TIME and DATE>

**Relevant Username:** <USERNAME>

_CWRC username if submitting an issue for the CWRC research space, or the Github username if using GitWriter._

### For issues related to uploading to CWRC Writer:

**The collection you were trying to add the object to:** <COLLECTION URL>

**The URL of the file  you were trying to upload:** <FILE URL>


## Additional information


**Remember to preview your issue for formatting/possible mistakes before submitting, thank you**



<Ignore these>

/label ~bug ~tools
/cc @zacanbot 
/assign @alliyya
