# Feedback

This is a repository for organizing issues related to data and tools for LINCS.

GitLab users can create an issue here directly but external users can use the [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html) feature:

* Send an email to [contact-project+calincs-admin-feedback-support@incoming.gitlab.com](mailto:contact-project+calincs-admin-feedback-support@incoming.gitlab.com)
* An issue will be automatically created in this project
* Comments on the issue will be emailed to the origin of the support request
* Subsequent email replies will become comments on the issue

## General steps on what to do before reporting

* Replicate it! Try to recreate the problem.
* Can you reproduce it in the exact same location?
* Can you reproduce it with a different file/form or in a different browser?

Make note of the steps involved. If it’s elusive, then start paying attention to what you’re doing to try to figure out what’s causing it. If it’s elusive, take screenshots of any error messages or weird behaviour as soon as they occur.

If it’s a problem with a **networked tool** take note of date and time of problems.

If you can replicate it, reach out and see if someone else can replicate it! If so, you can join forces in troubleshooting.

### Troubleshoot according to tried-and-true fixes

#### If a desktop tool

1. Shut down the tool, then restart it.
1. If that doesn’t fix the issue, restart your machine.

#### If a browser-based tool

1. Try replicating the issue in another browser. If you can replicate it in another browser, report it following the format outlined below. (preferably to use a browser that you have not used in a while, or one where you first clear caching)  
1. If the problem can’t be reproduced in another browser,
    * Restart the current  browser and see if that fixes it. If not, do the following
        * Clear cache and cookies
        * Restart the browser
        * Try again
    * If this doesn’t work,
        * disable browser extensions,
        * restart browser and
        * try again. Restart machine

If these steps don’t fix it, then report it, providing the following information where relevant, and create an issue [here](https://gitlab.com/calincs/admin/feedback/-/issues/new?issuable_template=tool_bugs).
